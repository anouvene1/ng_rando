import { Component } from '@angular/core';

@Component({
  selector: 'rando-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'NGRANDO';
}
