import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { HikeListComponent } from './hike/hike-list.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { HikeDetailsComponent } from './hike/hike-details.component';
import { ContactComponent } from './contact/contact.component';
import { MapComponent } from './map/map.component';

const ROUTES: Routes = [
    {path:"home", component: HomeComponent},
    {path:"hikes", component: HikeListComponent},
    {path:"hikes/:id", component: HikeDetailsComponent},
    {path:"contact", component: ContactComponent},
    {path:"about", component: MapComponent},
    {path:"", redirectTo: "home", pathMatch: "full"},
    {path:"**", component: PageNotFoundComponent}
];

export const RANDO_ROUTES = RouterModule.forRoot(ROUTES);