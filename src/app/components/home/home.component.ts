import { Component, OnInit } from '@angular/core';
import { Hike } from '../../shared/models/hike';

@Component({
  selector: 'rando-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }

  ngOnInit() {}

}
