import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ContactService } from './contact.service';

@Component({
  selector: 'rando-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  constructor(private contactService: ContactService) { }

  ngOnInit() { 
  }

  getErrorSubject(): string {
    const msg: string = "Sujet manquant !!";
    return msg;
  }

  sendMessage(form: NgForm) {
    console.log(form.value);
    this.contactService.postContactForm(form.value);
  }

}
