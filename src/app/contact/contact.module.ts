import { NgModule } from '@angular/core';

import { CoreModule } from '../shared/modules/core.module';
import { ContactComponent } from './contact.component';

@NgModule({
  declarations: [ContactComponent],
  imports: [
    CoreModule
  ]
})
export class ContactModule { }
