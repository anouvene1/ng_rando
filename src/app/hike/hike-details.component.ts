import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Hike } from '../shared/models/hike';
import { HikeService } from './hike.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'rando-hike-details',
  templateUrl: './hike-details.component.html',
  styleUrls: ['./hike-details.component.css']
})
export class HikeDetailsComponent implements OnInit {
  private randoId: number;

  public currentHike: Hike;
  public hike$: Observable<Hike>;
  public hikesCount: number = 0;

  private allHikes: Hike[] = [];
  
  constructor(private route: ActivatedRoute, private router: Router, private hikeService: HikeService) { }

  ngOnInit() {
    this.route.paramMap.subscribe( (params: ParamMap) => {
      this.randoId = +params.get("id");

      // Get hike by id
      this.hike$ = this.hikeService.getKikeById(this.randoId);

      // Get the current hike
      this.hike$.subscribe( (hike: Hike) => {
        this.currentHike = hike;
      });
    });

    // Get all hikes
    this.hikeService.getHikesFromCache().subscribe( (hikes: Hike[]) => {
      if(hikes) {
        this.allHikes = hikes;
      } else {
        this.allHikes = [];
      }
    });

  }

  goBack(): void {
    this.router.navigate(["/hikes"]);
  }

  goPrevious(currentHikeId: number) {
    let currentHikePos: number;
    currentHikePos = this.allHikes.findIndex((hike: Hike) => this.currentHike.id === hike.id);

    if(currentHikePos !== -1 && (currentHikePos + 1) < this.allHikes.length) {
      let nextHike: Hike = this.allHikes[currentHikePos + 1];
      this.router.navigate(["/hikes", nextHike.id])
    }
  }

}
