import { Pipe, PipeTransform } from '@angular/core';
import { Hike } from '../shared/models/hike';

@Pipe({
  name: 'hikeFilter'
})
export class HikeFilterPipe implements PipeTransform {

  transform(hikes: Hike[], searchTerm: string = ""): Hike[] {
    if(searchTerm !== "") {
      return hikes.filter((hike: Hike) => {
        return (hike.name.toLocaleLowerCase().includes(searchTerm.trim().toLowerCase())
        || hike.description.toLocaleLowerCase().includes(searchTerm.trim().toLowerCase()));
      });
    } else {
      return hikes;
    }
  }

}
