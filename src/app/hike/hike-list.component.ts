import { Component, OnInit } from '@angular/core';
import { HikeService } from './hike.service';
import { Hike } from '../shared/models/hike';


@Component({
  selector: 'rando-hike-list',
  templateUrl: './hike-list.component.html',
  styleUrls: ['./hike-list.component.css']
})
export class HikeListComponent implements OnInit {
  hikes: Hike[];
  private static countClick: number = 1;

  constructor(private hikeService: HikeService) { }

  ngOnInit() {
    this.hikeService.getHikesFromCache().subscribe( (hikes: Hike[]) => {
      if(hikes) {
        this.hikes = hikes;
      } else {
        this.hikes = [];
      }
    });
  }

  addHikeToFavouriteOnEmit(hikeEmitted: Hike): void {
    console.log(hikeEmitted);
  }

}
