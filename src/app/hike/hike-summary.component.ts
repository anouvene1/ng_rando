import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Hike } from '../shared/models/hike';
import { HikeService } from './hike.service';

@Component({
  selector: 'rando-hike-summary',
  templateUrl: './hike-summary.component.html',
  styleUrls: ['./hike-summary.component.css']
})
export class HikeSummaryComponent implements OnInit {  

  constructor() { }

  @Input("h") hike: Hike;
  @Output("favouriteHikeEmitted") emitter =  new EventEmitter<Hike>();

  ngOnInit() {}

  toggleHikeToFavourite(favouriteHike: boolean): void {
    if(favouriteHike) {
      this.emitter.emit(this.hike);
    }
  }



}
