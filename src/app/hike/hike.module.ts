import { NgModule } from '@angular/core';

import { CoreModule } from '../shared/modules/core.module';
import { HikeDetailsComponent } from './hike-details.component';
import { HikeFilterPipe } from './hike-filter.pipe';
import { HikeSummaryComponent } from './hike-summary.component';
import { HikeListComponent } from './hike-list.component';

@NgModule({
  declarations: [
    HikeDetailsComponent, 
    HikeFilterPipe, 
    HikeSummaryComponent,
    HikeListComponent
  ],
  imports: [
    CoreModule
  ],
  exports: [],
  providers: []
})
export class HikeModule { }
