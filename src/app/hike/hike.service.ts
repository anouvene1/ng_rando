
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Hike } from '../shared/models/hike';
import { Observable, of } from 'rxjs';
import { map, catchError, publishReplay, refCount } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class HikeService {

  private data: any;
  private observable: Observable<Hike[]>;

  private observableDetails: Observable<Hike>;

  constructor(private httpClient: HttpClient) {}

  getHikes(): Observable<Hike[]> {
    //const headers = new HttpHeaders({'Content-Type':'application/json', 'Access-Control-Allow-Origin':'*'});
    //return this.httpClient.get<Hike[]>('app/api/hikes.json',  {headers: headers}); 
    // voir la config pour le path "app/api/..." dans angular.json section "assets"
    return this.httpClient.get<Hike[]>('app/api/hikes.json');
  }

  getHikesFromCache() {       
    if(this.data) {
        return of(this.data);
    } else if(this.observable) {
        return this.observable;
    } else {
         this.observable = this.httpClient
                                  .get<Hike[]>('app/api/hikes.json')
                                  .pipe(
                                    map(data => data),
                                    publishReplay(1), // http cache
                                    refCount(), // returns an observable that maintains a reference count of subscribers
                                    catchError(error =>{
                                          let errorMessage = `Une erreur ${error.status} est survenue en tentant de joindre ${error.url}`;
                                          return Observable.throw(errorMessage);
                                    })
                                  );

        return this.observable;
    }
  }


  getKikeById(id: number): Observable<Hike> {
    return this.getHikesFromCache().pipe(
      map(hikes => hikes.find(hike => hike.id === id))
    );
  }


  getHikesLength(): number {
    let totalHikes: number = 0;
    this.getHikes().subscribe((hikes: Hike[])=> {
      totalHikes = hikes.length;
    });

    return totalHikes;
  }

}
