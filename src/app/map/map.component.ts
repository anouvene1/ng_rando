import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'rando-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  title: string;
  lat: number;
  lng: number;
  zoom: number;

  constructor() { }

  ngOnInit() {
    this.title = 'My first AGM project';
    this.lat = 45.772192 ;
    this.lng = 4.863135;
    this.zoom = 15;
  }

}
