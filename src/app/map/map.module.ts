import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AgmCoreModule } from '@agm/core';
import { MapComponent } from './map.component';

@NgModule({
  declarations: [MapComponent],
  imports: [
    CommonModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBhQERul6mLpJtviWqnrdq4ph2k6G7lk8Q'
    })
  ],
  exports: [AgmCoreModule]
})
export class MapModule { 

}
