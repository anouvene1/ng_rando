
// Core
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';


// Modules
import { RANDO_ROUTES } from '../../app.routing';
import { LayoutModule } from './layout.module';
import { TopbarComponent } from '../components/topbar/topbar.component';
import { FooterComponent } from '../components/footer/footer.component';


// Services

// Guards

// Interceptors

// Components


// Components
const COMPONENTS = [
  TopbarComponent,
  FooterComponent
];

@NgModule({
  declarations: COMPONENTS,
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RANDO_ROUTES,
    LayoutModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule, // Penser à bien exporter ce module car les autres modules l'utilisent !!!
    LayoutModule, // Idem
    HttpClientModule,
    ...COMPONENTS
  ],
  providers: []
})
export class CoreModule { }
